# 20230908-ChimezieOrji-NYCSchools


## Name
NYC Schools coding assessment

##Description
This app will get the list of high schools from the API provided. User can click on the Details button to see the SAT scores for that school.


##Technologies 
Jetpack Compose
Kotlin Language features
Hilt Dependency injection 
MVVM Architecture 
TDD Unit testing 
Retrofit, okhttp, Coroutines 

