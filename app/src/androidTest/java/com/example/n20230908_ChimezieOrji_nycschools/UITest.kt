package com.example.n20230908_ChimezieOrji_nycschools

import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.n20230908_ChimezieOrji_nycschools.view.HomeScreen
import com.example.n20230908_ChimezieOrji_nycschools.view.MainActivity
import com.example.n20230908_ChimezieOrji_nycschools.view.Screen
import com.example.n20230908_ChimezieOrji_nycschools.view.listschools.ListSchoolsScreen
import com.example.n20230908_ChimezieOrji_nycschools.view.ui.theme.ChimezieOrjiNYCSchoolsTheme
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UiTest {

    //mockk the nave controller and run a test to verify navigation between screens

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()
    //val navcontroller

    @Before
    fun setUp(){
        composeTestRule.setContent {
            val navController = rememberNavController()
            ChimezieOrjiNYCSchoolsTheme{
                NavHost(
                    navController = navController,
                    startDestination = Screen.HomeScreen.route
                ){
                    composable(route = Screen.HomeScreen.route){
                        HomeScreen(navController = navController)
                    }
                    composable(route = Screen.ListSchoolsScreen.route){
                        ListSchoolsScreen(navController = navController)
                    }
                }
            }
        }
    }

    @Test
    fun home_screen_to_schools_screen_change() {

        composeTestRule.onNodeWithText("List of NYC schools").assertExists()
        composeTestRule.onNodeWithText("List of NYC schools").performClick()
        composeTestRule.onNodeWithText("List Schools Screen").assertExists()
    }

}
