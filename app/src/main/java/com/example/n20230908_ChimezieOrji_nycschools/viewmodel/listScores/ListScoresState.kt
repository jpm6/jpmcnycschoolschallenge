package com.example.n20230908_ChimezieOrji_nycschools.viewmodel.listScores

import com.example.n20230908_ChimezieOrji_nycschools.model.ScoresItem

data class ListScoresState(

    val isLoading: Boolean = false,
    val schools: List<ScoresItem> = emptyList(),
    val error: String = ""
)