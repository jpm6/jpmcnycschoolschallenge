package com.example.n20230908_ChimezieOrji_nycschools.viewmodel.listScores

import com.example.n20230908_ChimezieOrji_nycschools.common.Resource
import com.example.n20230908_ChimezieOrji_nycschools.model.ScoresItem
import com.example.n20230908_ChimezieOrji_nycschools.model.repository.SchoolsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetScoresUseCase @Inject constructor(
    private val repository: SchoolsRepository
) {

    operator fun invoke(dbn: String): Flow<Resource<List<ScoresItem>>> = flow {
        try {
            emit(Resource.Loading<List<ScoresItem>>())
            val scores = repository.getScores(dbn = dbn)
            //can add edge cases here to handel schools that dont have scores
            emit(Resource.Success<List<ScoresItem>>(scores))
        } catch ( e: HttpException){
            emit(Resource.Error<List<ScoresItem>>(e.localizedMessage ?: "An unexpected error"))
        } catch (e : IOException) {
            emit(Resource.Error<List<ScoresItem>>("Check internet connection"))
        }
    }
}