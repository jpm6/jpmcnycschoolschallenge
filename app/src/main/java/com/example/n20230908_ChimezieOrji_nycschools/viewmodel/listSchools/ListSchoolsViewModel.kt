package com.example.n20230908_ChimezieOrji_nycschools.viewmodel.listSchools

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.n20230908_ChimezieOrji_nycschools.common.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ListSchoolsViewModel @Inject constructor(
    private val getSchoolsUseCase: GetSchoolsUseCase
): ViewModel() {

    private val _state = mutableStateOf(ListSchoolsState())
    val state: State<ListSchoolsState> = _state

    init {
        getSchools()
    }

    private fun getSchools() {
        getSchoolsUseCase().onEach { result->
            when(result){
                is Resource.Success -> {
                    _state.value = ListSchoolsState(schools = result.data ?: emptyList())
                }
                is Resource.Error -> {
                    _state.value = ListSchoolsState(error = result.message ?: "An unexpected error occured")
                }
                is Resource.Loading -> {
                    _state.value = ListSchoolsState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }
}