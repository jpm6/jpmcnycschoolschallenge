package com.example.n20230908_ChimezieOrji_nycschools.viewmodel.listSchools

import com.example.n20230908_ChimezieOrji_nycschools.model.SchoolsItem

data class ListSchoolsState(

    val isLoading: Boolean = false,
    val schools: List<SchoolsItem> = emptyList(),
    val error: String = ""
)
