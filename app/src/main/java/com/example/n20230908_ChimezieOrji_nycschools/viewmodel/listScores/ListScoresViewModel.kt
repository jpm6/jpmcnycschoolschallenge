package com.example.n20230908_ChimezieOrji_nycschools.viewmodel.listScores

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.n20230908_ChimezieOrji_nycschools.common.Constants
import com.example.n20230908_ChimezieOrji_nycschools.common.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ListScoresViewModel @Inject constructor(
    private val getScoresUseCase: GetScoresUseCase,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    //using viewmodel to keep the UIState
    //other Business logic moved to use cases

    private val _state = mutableStateOf(ListScoresState())
    val state: State<ListScoresState> = _state

    init {
        savedStateHandle.get<String>(Constants.PARAM_SCHOOL_DBN)?.let { dbn ->
            getScores(dbn)
        }
    }

    private fun getScores(dbn: String) {
        getScoresUseCase(dbn).onEach { result->
            when(result){
                is Resource.Success -> {
                    _state.value = ListScoresState(schools = result.data ?: emptyList())
                }
                is Resource.Error -> {
                    _state.value = ListScoresState(error = result.message ?: "An unexpected error occured")
                }
                is Resource.Loading -> {
                    _state.value = ListScoresState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }
}