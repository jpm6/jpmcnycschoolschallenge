package com.example.n20230908_ChimezieOrji_nycschools.viewmodel.listSchools

import com.example.n20230908_ChimezieOrji_nycschools.model.SchoolsItem
import com.example.n20230908_ChimezieOrji_nycschools.model.repository.SchoolsRepository
import com.example.n20230908_ChimezieOrji_nycschools.common.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetSchoolsUseCase @Inject constructor(
    private val repository: SchoolsRepository
) {

    operator fun invoke(): Flow<Resource<List<SchoolsItem>>> = flow {
        try {
            emit(Resource.Loading<List<SchoolsItem>>())
            val schools = repository.getSchools()
            emit(Resource.Success<List<SchoolsItem>>(schools))
        } catch ( e: HttpException){
            emit(Resource.Error<List<SchoolsItem>>(e.localizedMessage ?: "An unexpected error"))
        } catch (e : IOException) {
            emit(Resource.Error<List<SchoolsItem>>("Check internet connection"))
        }
    }
}