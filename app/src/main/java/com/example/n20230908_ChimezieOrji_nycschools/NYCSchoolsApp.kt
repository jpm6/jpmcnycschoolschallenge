package com.example.n20230908_ChimezieOrji_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolsApp: Application() {
}