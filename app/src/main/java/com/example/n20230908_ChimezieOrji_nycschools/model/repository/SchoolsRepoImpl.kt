package com.example.n20230908_ChimezieOrji_nycschools.model.repository

import com.example.n20230908_ChimezieOrji_nycschools.model.SchoolsItem
import com.example.n20230908_ChimezieOrji_nycschools.model.ScoresItem
import com.example.n20230908_ChimezieOrji_nycschools.model.remote.SchoolsApi

class SchoolsRepoImpl(
    private val api: SchoolsApi
): SchoolsRepository {

    override suspend fun getSchools(): List<SchoolsItem> {
        return api.getSchools()
    }

    override suspend fun getScores(dbn: String): List<ScoresItem> {
        return api.getScores(dbn = dbn)
    }
}