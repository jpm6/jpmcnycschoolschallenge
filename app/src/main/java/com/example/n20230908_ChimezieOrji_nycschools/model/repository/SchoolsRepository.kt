package com.example.n20230908_ChimezieOrji_nycschools.model.repository

import com.example.n20230908_ChimezieOrji_nycschools.model.SchoolsItem
import com.example.n20230908_ChimezieOrji_nycschools.model.ScoresItem

interface SchoolsRepository {

    suspend fun getSchools(): List<SchoolsItem>

    suspend fun getScores(dbn: String): List<ScoresItem>
}