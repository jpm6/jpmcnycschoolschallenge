package com.example.n20230908_ChimezieOrji_nycschools.model



data class SchoolsItem(

    val dbn: String,
    val school_name: String,
    val overview_paragraph: String,
)