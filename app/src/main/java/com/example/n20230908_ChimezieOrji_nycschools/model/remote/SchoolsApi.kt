package com.example.n20230908_ChimezieOrji_nycschools.model.remote

import com.example.n20230908_ChimezieOrji_nycschools.common.Constants
import com.example.n20230908_ChimezieOrji_nycschools.model.SchoolsItem
import com.example.n20230908_ChimezieOrji_nycschools.model.ScoresItem
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsApi {

    @GET(Constants.NYC_SCHOOLS_ENDPOINT)
    suspend fun getSchools(): List<SchoolsItem>

    @GET(Constants.NYC_SCORES_ENDPOINT)
    suspend fun getScores(
        @Query("dbn") dbn: String
    ): List<ScoresItem>
}

