package com.example.n20230908_ChimezieOrji_nycschools.view.listscores

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.n20230908_ChimezieOrji_nycschools.viewmodel.listScores.ListScoresViewModel

@Composable
fun ListScoresScreen(
    viewModel: ListScoresViewModel = hiltViewModel(),

    ){
    val state = viewModel.state.value
    val listItems = state.schools

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = "List Scores Screen",
            modifier = Modifier.align(Alignment.TopCenter)
        )
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
        ) {
            items(listItems) {scores ->
                ListScoresItem(scores = scores)
            }
        }

        if (state.error.isNotBlank()) {
            Text(
                text = state.error,
                color = MaterialTheme.colors.error,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .align(Alignment.Center)
            )
        }
        if(state.isLoading) {
            CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
        }
    }
}