package com.example.n20230908_ChimezieOrji_nycschools.view

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController


@Composable
fun HomeScreen(navController: NavController){
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Row (modifier = Modifier.align(Alignment.Center)){
            Button(onClick = {
                navController.navigate(Screen.ListSchoolsScreen.route)
            },
            ) {
                Text(text = "")
            }
        }
        Text(modifier = Modifier.align(Alignment.TopCenter), text = "home screen")
        Spacer(modifier = Modifier.height(8.dp))

    }
}


sealed class Screen(val route: String) {
    object HomeScreen: Screen("home_screen")
    object ListSchoolsScreen: Screen("list_schools_screen")
    object ListScoresScreen: Screen("list_scores_screen")
}