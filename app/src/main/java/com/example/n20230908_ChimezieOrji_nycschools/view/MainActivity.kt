package com.example.n20230908_ChimezieOrji_nycschools.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.n20230908_ChimezieOrji_nycschools.view.listschools.ListSchoolsScreen
import com.example.n20230908_ChimezieOrji_nycschools.view.listscores.ListScoresScreen
import com.example.n20230908_ChimezieOrji_nycschools.view.ui.theme.ChimezieOrjiNYCSchoolsTheme
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ChimezieOrjiNYCSchoolsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                )  {

                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screen.ListSchoolsScreen.route
                    ) {
                        composable(
                            route = Screen.HomeScreen.route
                        ) {
                            HomeScreen(navController = navController)
                        }
                        composable(
                            route = Screen.ListSchoolsScreen.route
                        ) {
                            ListSchoolsScreen(navController = navController)
                        }
                        composable(
                            route = Screen.ListScoresScreen.route + "/{dbn}"
                        ) {
                            ListScoresScreen()
                        }
                    }
                }
            }
        }
    }
}