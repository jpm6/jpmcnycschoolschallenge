package com.example.n20230908_ChimezieOrji_nycschools.view.listscores

import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.example.n20230908_ChimezieOrji_nycschools.model.ScoresItem

@Composable
fun ListScoresItem (
    scores: ScoresItem,
) {
    Column (
        modifier = Modifier
            .fillMaxWidth()
            .padding(20.dp),
        verticalArrangement = Arrangement.SpaceBetween

    ) {
        Text(
            text = scores.school_name,
            style = MaterialTheme.typography.body1,
            overflow = TextOverflow.Ellipsis
        )
        Text(text = "MATH: ${scores.sat_math_avg_score}")
        Text(text = "WRITING: ${scores.sat_writing_avg_score}")
        Text(text = "Reading: ${scores.sat_critical_reading_avg_score}")

    }
}