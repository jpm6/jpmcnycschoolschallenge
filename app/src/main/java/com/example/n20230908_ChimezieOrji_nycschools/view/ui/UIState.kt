package com.example.n20230908_ChimezieOrji_nycschools.view.ui

sealed class UIState {
    object LOADING:UIState()
    data class SUCCESS<T>(val response: T):UIState()
    data class ERROR(val error: Exception): UIState()
}