package com.example.n20230908_ChimezieOrji_nycschools.view.listschools

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.n20230908_ChimezieOrji_nycschools.view.Screen
import com.example.n20230908_ChimezieOrji_nycschools.viewmodel.listSchools.ListSchoolsViewModel

@Composable
fun ListSchoolsScreen(
    viewModel: ListSchoolsViewModel = hiltViewModel(),
    navController: NavController

    ){
    val state = viewModel.state.value
    val listItems = state.schools

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = "List Schools Screen",
            modifier = Modifier.align(Alignment.TopCenter)
        )
        Spacer(modifier = Modifier.height(16.dp))
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
        ) {
            items(listItems) {school ->
                ListSchoolsItem(
                    school = school,
                    onItemClick = {
                        navController.navigate(Screen.ListScoresScreen.route + "/${school.dbn}")
                    }
                )
            }
        }

        if (state.error.isNotBlank()) {
            Text(
                text = state.error,
                color = MaterialTheme.colors.error,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .align(Alignment.Center)
            )
        }
        if(state.isLoading) {
            CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
        }
    }
}
