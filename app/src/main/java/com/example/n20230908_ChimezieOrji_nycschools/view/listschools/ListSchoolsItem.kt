package com.example.n20230908_ChimezieOrji_nycschools.view.listschools

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.example.n20230908_ChimezieOrji_nycschools.model.SchoolsItem

@Composable
fun ListSchoolsItem (
    school: SchoolsItem,
    onItemClick: (SchoolsItem) -> Unit,
) {
    Column (
        modifier = Modifier
            .fillMaxWidth()
            .padding(20.dp),
        verticalArrangement = Arrangement.SpaceBetween,


    ) {
        Text(
            text = school.school_name,
            style = MaterialTheme.typography.h5,
            overflow = TextOverflow.Ellipsis,
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = "Description: ${school.overview_paragraph}",
            style = MaterialTheme.typography.body1,
            overflow = TextOverflow.Ellipsis
        )
        Spacer(modifier = Modifier.height(16.dp))
        Button(onClick = { onItemClick(school) }) {
            Text(text = "Details")
        }

    }
}