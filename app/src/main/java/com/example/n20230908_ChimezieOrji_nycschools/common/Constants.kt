package com.example.n20230908_ChimezieOrji_nycschools.common

object Constants {
    const val BASEURL = "https://data.cityofnewyork.us/resource/"
    const val NYC_SCHOOLS_ENDPOINT = "s3k6-pzi2.json"
    const val NYC_SCORES_ENDPOINT = "f9bf-2cp4.json"
    const val PARAM_SCHOOL_DBN = "dbn"
}