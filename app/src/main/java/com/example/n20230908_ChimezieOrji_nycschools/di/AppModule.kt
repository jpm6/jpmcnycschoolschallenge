package com.example.n20230908_ChimezieOrji_nycschools.di

import com.example.n20230908_ChimezieOrji_nycschools.common.Constants
import com.example.n20230908_ChimezieOrji_nycschools.model.remote.SchoolsApi
import com.example.n20230908_ChimezieOrji_nycschools.model.repository.SchoolsRepoImpl
import com.example.n20230908_ChimezieOrji_nycschools.model.repository.SchoolsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesSchoolsApi(): SchoolsApi {
        return Retrofit.Builder()
            .baseUrl(Constants.BASEURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SchoolsApi::class.java)
    }

    @Provides
    @Singleton
    fun providesSchoolsRepository(api: SchoolsApi): SchoolsRepository {
        return SchoolsRepoImpl(api)
    }
}