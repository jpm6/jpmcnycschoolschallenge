package com.example.n20230908_ChimezieOrji_nycschools

import com.example.n20230908_ChimezieOrji_nycschools.model.SchoolsItem
import com.example.n20230908_ChimezieOrji_nycschools.model.ScoresItem
import com.example.n20230908_ChimezieOrji_nycschools.model.repository.SchoolsRepository

class FakeApi : SchoolsRepository {

    private val schools = listOf<SchoolsItem>()
    private val scores = listOf<ScoresItem>()

    override suspend fun getSchools(): List<SchoolsItem> {

        return schools
    }

    override suspend fun getScores(dbn: String): List<ScoresItem> {
        return scores
    }

}
