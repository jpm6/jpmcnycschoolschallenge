package com.example.n20230908_ChimezieOrji_nycschools

import com.example.n20230908_ChimezieOrji_nycschools.viewmodel.listSchools.GetSchoolsUseCase
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test

class GetSchoolsUseCaseTest{

    private lateinit var getSchoolsUseCase: GetSchoolsUseCase
    private lateinit var fakeApi: FakeApi
    //private val mockRepository = mockk<SchoolsRepository>(relaxed = true)
    //setup a fakeapi to mock the api response mockk is giving errors so i have a manual implementation of a fakeApi

    @Before
    fun setUp(){
        //val mockapi = mockk<SchoolsRepository>()
        //here we would setup the mockapi
        fakeApi = FakeApi()
        getSchoolsUseCase = GetSchoolsUseCase(fakeApi)
    }


    //@OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `get schools use case returns a value`() {


        val schoolsList = getSchoolsUseCase.invoke()

        assertThat(schoolsList).isNotNull()
    }
}

