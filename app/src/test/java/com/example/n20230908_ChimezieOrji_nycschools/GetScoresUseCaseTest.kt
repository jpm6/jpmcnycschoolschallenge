package com.example.n20230908_ChimezieOrji_nycschools

import com.example.n20230908_ChimezieOrji_nycschools.viewmodel.listScores.GetScoresUseCase
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test

class GetScoresUseCaseTest {

    private lateinit var getScoresUseCase: GetScoresUseCase
    private lateinit var fakeApi: FakeApi

    @Before
    fun setUp(){
        fakeApi = FakeApi()
        getScoresUseCase = GetScoresUseCase(fakeApi)
    }

    @Test
    fun `get scores use case returns success`(){

        val scores = getScoresUseCase.invoke("111")

        assertThat(scores).isNotNull()
    }


}
